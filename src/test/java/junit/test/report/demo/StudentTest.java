package junit.test.report.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StudentTest {

	private Student s1;
	
	@Test
	void testSetFirstName() {
		s1 = new Student("John", "Smith");
		s1.setFirstName("Joe");
		assertEquals("Joe", s1.getFirstName() );
	}
	
	@Test
	void testSetLastName() {
		s1 = new Student ("John", "Smith");
		s1.setLastName("Allen");
		assertEquals("Allen", s1.getLastName() );
	}
	
}
